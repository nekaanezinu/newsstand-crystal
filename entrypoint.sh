#!/bin/bash
# Create .env file
echo "DB_HOST=${DB_HOST}" > /newsstand-crystal/.env
echo "DB_USER=${DB_USER}" >> /newsstand-crystal/.env
echo "DB_PASSWORD=${DB_PASSWORD}" >> /newsstand-crystal/.env
echo "DB_DBNAME=${DB_DBNAME}" >> /newsstand-crystal/.env
echo "DB_PORT=${DB_PORT}" >> /newsstand-crystal/.env
