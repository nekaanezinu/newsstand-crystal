# Builder stage
FROM arm64v8/ubuntu:22.04 AS builder
RUN apt-get update && apt-get install -y openssl libssl-dev libz-dev libevent-2.1-7 gcc llvm libgc-dev libpcre2-dev libevent-dev && rm -rf /var/lib/apt/lists/*
WORKDIR /newsstand-crystal
COPY dist/* ./
RUN chmod +x /newsstand-crystal/link_command.sh
RUN ./link_command.sh

# Runtime stage
FROM arm64v8/ubuntu:22.04
RUN apt-get update && apt-get install -y openssl libssl-dev libz-dev libevent-2.1-7 gcc llvm libgc-dev libpcre2-dev libevent-dev && rm -rf /var/lib/apt/lists/*
WORKDIR /newsstand-crystal
COPY --from=builder /newsstand-crystal/ ./

# Set up the entrypoint and command
COPY entrypoint.sh /newsstand-crystal/entrypoint.sh
RUN chmod +x /newsstand-crystal/entrypoint.sh
# CMD ./entrypoint.sh && tail -F /dev/null
CMD ./entrypoint.sh && ./main