none:
	echo "none"

install_deps:
	apt-get update && apt-get install -y openssl libssl-dev libz-dev libevent-2.1-7 gcc llvm libgc-dev libpcre2-dev libevent-dev

build:
	shards install
	if [[ -f "dist/link_command.sh" ]]; then \
		rm "dist/link_command.sh"; \
	fi
	crystal build --release src/main.cr --cross-compile --target "aarch64-unknown-linux-gnu" > ./dist/link_command.sh
	mv main.o dist/

run:
	./sentry -r "./main"

