class State
  property user : User

  def initialize(context)
    begin
      secret = context.request.headers.get("X-Auth-Secret").first
      tmp_user = User.find_by_secret(secret)
    rescue e : DB::NoResultsError
      context.response.status = HTTP::Status::UNAUTHORIZED
      context.response.print "{\"error\" : \"User not found\"}"
      raise SilentError.new(e)
    rescue e : KeyError
      context.response.status = HTTP::Status::BAD_REQUEST
      context.response.print "{\"error\" : \"Missing authorization header: X-Auth-Secret\"}"
      raise SilentError.new(e)
    end

    @user = tmp_user.not_nil!
  end
end

class SilentError < Exception
  getter error : Exception

  def initialize(@error : Exception)
    super("Known SilentError: #{@error}")
  end
end
