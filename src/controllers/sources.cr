require "../mutations/sources"
require "./shared/mutation"

module Controllers::Sources
  def self.handle_request(context : HTTP::Server::Context, state : State)
    case context.request.method
    when "GET"
      get_all(context)
    when "POST"
      create(context, state)
    end
  end

  def self.get_all(context)
    context.response.print Source.all.to_json
  end

  def self.create(context, state)
    body = context.request.body || ""
    Controllers::Shared::Mutation.call(context) do
      params = JSON.parse(body)
      source = Mutations::Sources::Create.run(params, state.user)
      context.response.print source.to_json
    end
  end
end
