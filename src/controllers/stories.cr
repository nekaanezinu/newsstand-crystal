require "../mutations/stories"
require "./shared/mutation"

module Controllers::Stories
  def self.handle_request(context : HTTP::Server::Context, state : State)
    case context.request.method
    when "GET"
      get_all(context)
    when "POST"
      create(context, state)
    when "PATCH"
      update(context, state)
    end
  end

  def self.handle_request_with_id(context)
    id = context.request.path.split("/").last.to_i
    case context.request.method
    when "GET"
      get_one(context, id)
    end
  end

  def self.get_all(context)
    context.response.print Story.all.to_json
  end

  def self.create(context, state)
    body = context.request.body || ""
    Controllers::Shared::Mutation.call(context) do
      params = JSON.parse(body)
      story = Mutations::Stories::Create.run(params, state.user)
      context.response.print story.to_json
    end
  end

  def self.get_one(context, id)
    story = Story.find(id.to_i)
    context.response.print story.to_json
  end

  def self.update(context, state)
    body = context.request.body || ""
    Controllers::Shared::Mutation.call(context) do
      params = JSON.parse(body)
      story = Mutations::Stories::Update.run(params, state.user)
      context.response.print story.to_json
    end
  end
end
