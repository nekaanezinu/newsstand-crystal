module Controllers::Shared::Mutation
  def self.call(context, &block)
    begin
      yield
    rescue e : Inputs::Shared::Errors::JsonMapError
      context.response.status = HTTP::Status::BAD_REQUEST
      context.response.print "{ \"errors\": #{e.errors.to_json} }"
    rescue JSON::ParseException
      context.response.status = HTTP::Status::BAD_REQUEST
      context.response.print "{ \"errors\": \"Invalid JSON\" }"
    end
  end
end
