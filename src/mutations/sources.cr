module Mutations::Sources
  module Create
    def self.run(params : JSON::Any, user : User) : Source
      source = Inputs::Sources::Create.new(params["attributes"]).to_model(user.id!)
      source.save
      source
    end
  end
end
