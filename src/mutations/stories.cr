module Mutations::Stories
  module Create
    def self.run(params : JSON::Any, user : User) : Story
      story = Inputs::Stories::Create.new(params["attributes"]).to_model(user.id!)
      story.save
      story
    end
  end

  module Update
    def self.run(params : JSON::Any, user : User) : Story
      story = Inputs::Stories::Update.new(params["attributes"]).to_model(user.id!)
      story.save
      story
    end
  end
end
