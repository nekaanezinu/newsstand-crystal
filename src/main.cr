require "db"
require "http/server"
require "time"

require "./db/main"
require "./models/user"
require "./models/story"
require "./inputs/stories"
require "./inputs/sources"
require "./inputs/shared/errors"
require "./controllers/sources"
require "./controllers/stories"
require "./state"

Database.setup_database

server = HTTP::Server.new do |context|
  state = begin
    State.new(context)
  rescue e : SilentError
    next
  end

  p state.user
  context.response.content_type = "application/json"
  case context.request.path
  when "/stories"
    Controllers::Stories.handle_request(context, state)
  when /^\/stories\/(\d+)$/
    Controllers::Stories.handle_request_with_id(context)
  when "/sources"
    Controllers::Sources.handle_request(context, state)
  else
    context.response.status = HTTP::Status::NOT_FOUND
    context.response.print "Not Found"
  end
end

address = server.bind_tcp("0.0.0.0", 8080)
puts "Listening on http://#{address}"
server.listen
