require "db"
require "pg"
require "dotenv"

Dotenv.load

CHECK_MIGRATIONS_TABLE_EXISTS_QUERY = <<-SQL
  SELECT EXISTS (
      SELECT FROM
          pg_catalog.pg_tables
      WHERE
          schemaname != 'pg_catalog' AND
          schemaname != 'information_schema' AND
          tablename  = 'migrations'
  );
SQL

CREATE_MIGRATIONS_QUERY = <<-SQL
  CREATE TABLE migrations (
      date BIGINT NOT NULL
  );
SQL

MIGRATIONS = {
  20240104152651 => <<-SQL,
    CREATE TABLE users (
        id SERIAL PRIMARY KEY,
        secret TEXT
    );
  SQL
  20240104152652 => <<-SQL,
    CREATE TABLE stories (
        id SERIAL PRIMARY KEY,
        score INT NOT NULL DEFAULT 0,
        title TEXT NOT NULL,
        url TEXT NOT NULL,
        external_id TEXT NOT NULL,
        favorited_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        user_id INT REFERENCES users(id)
    );
  SQL
  20240104152653 => <<-SQL,
    CREATE TABLE sources (
      id SERIAL PRIMARY KEY,
      name TEXT NOT NULL,
      url TEXT NOT NULL,
      user_id INT REFERENCES users(id)
    )
  SQL
  20240104152654 => <<-SQL,
    ALTER TABLE stories
    ADD COLUMN source_id INT;
  SQL
}

module Database
  DB_URL = "postgres://#{ENV["DB_USER"]}:#{ENV["DB_PASSWORD"]}@#{ENV["DB_HOST"]}:#{ENV["DB_PORT"]}/#{ENV["DB_DBNAME"]}"

  def self.connection
    DB.open(DB_URL) do |db|
      yield db
    end
  end

  def self.setup_database
    self.connection do |db|
      db.query CHECK_MIGRATIONS_TABLE_EXISTS_QUERY do |rs|
        rs.each do
          if !rs.read(Bool)
            p "migrations table not found, creating it"
            db.exec CREATE_MIGRATIONS_QUERY
          else
            p "migrations table found, skipping creation"
          end
        end
      end

      last_migration : Int64 = 0

      db.query(
        <<-SQL
          SELECT * FROM migrations ORDER BY date DESC LIMIT 1
        SQL
      ) do |rs|
        rs.each do
          last_migration = rs.read(Int64)
        end
      end

      MIGRATIONS.each do |date, sql|
        next if last_migration && last_migration >= date && p "skipping migration: #{date}"
        p "running migration #{date}"
        db.exec sql
        db.exec "INSERT INTO migrations (date) VALUES (#{date})"
        p "migration done: #{date}"
      end
    rescue e
      raise e
    end
  end
end
