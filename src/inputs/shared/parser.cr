require "json"

require "./errors"

module Inputs::Shared::Parser
  include Inputs::Shared::Errors

  private property json : JSON::Any
  property errors : Hash(String, String) = {} of String => String

  macro property_parser
    {% for prop, config in PROPERTY_CONFIGS %}
      property {{prop.id}} : {{config[:type].id}}

      def _parse_{{prop.id}} : {{config[:type].id}}
        value = json["{{prop.id}}"]?.to_s
        parsed_value =
          {% if config[:parser] %}
            {{config[:parser].id}}(value)
          {% else %}
            {% if config[:type].id == "String" %}
              _parse_string(value)
            {% elsif config[:type].id == "Int32" %}
              _parse_int(value)
            {% elsif config[:type].id == "Time" %}
              _parse_time(value)
            {% else %}
              raise "nothing matched for type: #{{{ config[:type] }}}"
            {% end %}
          {% end %}
        parsed_value.as({{ config[:type] }})
      rescue ex
        errors["{{prop.id}}"] = ex.message.to_s
        {% if config[:type].id == "String" %}
          ""
        {% elsif config[:type].id == "Int32" %}
          0
        {% elsif config[:type].id == "Time" %}
          Time.utc
        {% end %}
      end
    {% end %}

    def initialize(@json : JSON::Any)
      {% for prop, config in PROPERTY_CONFIGS %}
        @{{prop}} = _parse_{{prop}}
      {% end %}
      raise JsonMapError.new(errors) unless errors.empty?
    end
  end

  def _parse_int(value) : Int32
    value.to_s.to_i
  end

  def _parse_string(value) : String
    value.to_s
  end

  def _parse_time(value) : Time
    Time.parse(value.to_s, "%FT%TZ", Time::Location::UTC)
  end
end
