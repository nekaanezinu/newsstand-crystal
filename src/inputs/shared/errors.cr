module Inputs::Shared::Errors
  class JsonMapError < Exception
    getter errors : Hash(String, String)

    def initialize(@errors : Hash(String, String))
      super("Error mapping properties: #{@errors}")
    end
  end
end
