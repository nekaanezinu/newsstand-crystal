require "./shared/parser"
require "./shared/errors"

module Inputs
  module Stories
    include Inputs::Shared::Errors

    class Create
      include Inputs::Shared::Parser

      PROPERTY_CONFIGS = {
        score:        {type: Int32},
        title:        {type: String},
        url:          {type: String},
        external_id:  {type: String},
        favorited_at: {type: Time},
        source_id:    {type: Int32, parser: :parse_source_id},
      }

      property_parser

      def parse_source_id(value : String) : Int32
        Source.find(value.to_s.to_i).id!
      end

      def to_model(user_id : Int32) : Story
        Story.new(
          nil,
          @score,
          @title,
          @url,
          @external_id,
          @favorited_at,
          user_id,
          @source_id,
        )
      end
    end

    class Update
      include Inputs::Shared::Parser

      PROPERTY_CONFIGS = {
        id:           {type: Int32, parser: :parse_id},
        score:        {type: Int32},
        title:        {type: String},
        url:          {type: String},
        external_id:  {type: String},
        favorited_at: {type: Time},
        source_id:    {type: Int32, parser: :parse_source_id},
      }

      property_parser

      def parse_id(value : String) : Int32
        Story.find(value.to_s.to_i).id!
      end

      def parse_source_id(value : String) : Int32
        Source.find(value.to_s.to_i).id!
      end

      def to_model(user_id : Int32) : Story
        Story.new(
          @id,
          @score,
          @title,
          @url,
          @external_id,
          @favorited_at,
          user_id,
          @source_id,
        )
      end
    end
  end
end
