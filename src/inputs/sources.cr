require "./shared/parser"
require "./shared/errors"

module Inputs
  module Sources
    include Inputs::Shared::Errors

    class Create
      include Inputs::Shared::Parser

      PROPERTY_CONFIGS = {
        name: {type: String},
        url:  {type: String},
      }
      property_parser

      def to_model(user_id : Int32) : Source
        Source.new(
          nil,
          @name,
          @url,
          user_id
        )
      end
    end
  end
end
