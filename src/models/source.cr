require "../db/main"
require "./shared/id"

class Source
  include Models::Shared::Id

  include JSON::Serializable

  property id : Int32?
  property name : String?
  property url : String?
  property user_id : Int32

  def initialize(@id : Int32?, @name : String?, @url : String?, @user_id : Int32)
  end

  def self.find(id : Int32) : Source
    Database.connection do |db|
      db.query_one("SELECT * FROM sources WHERE id = $1", id) do |row|
        Source.new(row.read(Int32), row.read(String), row.read(String), row.read(Int32))
      end
    end
  end

  def self.all
    sources = [] of Source
    Database.connection do |db|
      db.query_all("SELECT * FROM sources") do |row|
        sources << Source.new(row.read(Int32), row.read(String), row.read(String), row.read(Int32))
      end
    end
    sources
  end

  def save
    query : String? = nil
    if @id
      query = "UPDATE sources SET name = '#{@name}', url = '#{@url}', user_id = #{@user_id} WHERE id = #{@id} RETURNING id"
    else
      query = "INSERT INTO sources (name, url, user_id) VALUES ('#{@name}', '#{@url}', #{@user_id}) RETURNING id"
    end
    Database.connection do |db|
      res = db.query_one(query) do |rs|
        @id = rs.read(Int32)
      end
    end
  end
end
