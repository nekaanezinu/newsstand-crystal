require "../db/main"
require "./source"
require "./shared/id"

class Story
  include Models::Shared::Id

  include JSON::Serializable

  property id : Int32?
  property score : Int32
  property title : String
  property url : String
  property external_id : String
  property favorited_at : Time
  property user_id : Int32
  property source_id : Int32

  def initialize(@id : Int32?, @score : Int32, @title : String, @url : String, @external_id : String, @favorited_at : Time, @user_id : Int32, @source_id : Int32)
  end

  def self.find(id : Int32) : Story
    Database.connection do |db|
      db.query_one("SELECT * FROM stories WHERE id = $1", id) do |row|
        Story.new(
          row.read(Int32),
          row.read(Int32),
          row.read(String),
          row.read(String),
          row.read(String),
          row.read(Time),
          row.read(Int32),
          row.read(Int32),
        )
      end
    end
  end

  def self.all
    Database.connection do |db|
      db.query_all("SELECT * FROM stories") do |row|
        Story.new(
          row.read(Int32),
          row.read(Int32),
          row.read(String),
          row.read(String),
          row.read(String),
          row.read(Time),
          row.read(Int32),
          row.read(Int32),
        )
      end
    end
  end

  def save
    query : String? = nil
    if @id
      query = "UPDATE stories SET score = #{@score}, title = '#{@title}', url = '#{@url}', external_id = '#{@external_id}', favorited_at = '#{@favorited_at}', user_id = #{@user_id}, source_id = #{@source_id} WHERE id = #{@id} RETURNING id"
    else
      query = "INSERT INTO stories (score, title, url, external_id, favorited_at, user_id, source_id) VALUES (#{@score}, '#{@title}', '#{@url}', '#{@external_id}', '#{@favorited_at}', #{@user_id}, #{@source_id}) RETURNING id"
    end
    Database.connection do |db|
      res = db.query_one(query) do |rs|
        @id = rs.read(Int32)
      end
    end
  end
end
