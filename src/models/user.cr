require "../db/main"
require "./shared/id"

class User
  include Models::Shared::Id

  include JSON::Serializable

  property id : Int32?
  property secret : String

  def initialize(@id : Int32?, @secret : String)
  end

  def self.find(id : Int32) : User
    Database.connection do |db|
      db.query_one("SELECT * FROM users WHERE id = $1", id) do |row|
        User.new(row.read(Int32), row.read(String))
      end
    end
  end

  def self.find_by_secret(secret : String) : User
    Database.connection do |db|
      db.query_one("SELECT * FROM users WHERE secret = $1", secret) do |row|
        User.new(row.read(Int32), row.read(String))
      end
    end
  end

  def self.all
    users = [] of User
    Database.connection do |db|
      db.query_all("SELECT * FROM users") do |row|
        users << User.new(row.read(Int32), row.read(String))
      end
    end
    users
  end

  def save
    query : String? = nil
    if @id
      query = "UPDATE users SET secret = '#{@secret}' WHERE id = #{@id} RETURNING id"
    else
      query = "INSERT INTO users (secret) VALUES ('#{secret}') RETURNING id"
    end
    Database.connection do |db|
      res = db.query_one(query) do |rs|
        @id = rs.read(Int32)
      end
    end
  end
end
