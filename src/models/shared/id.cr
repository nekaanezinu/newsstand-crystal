module Models::Shared::Id
  def id! : Int32
    raise "Id is nil" if id.nil?
    id.not_nil!
  end
end
